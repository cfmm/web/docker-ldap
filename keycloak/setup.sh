#!/usr/bin/env bash
set -e
set -x

# This script sets up keycloak instance to be compatible with ldap and dcm4chee docker images
keycloak_url=http://127.0.0.1:8080

env

function is_keycloak_running {
    local http_code

    http_code=$(curl -s -o /dev/null -w "%{http_code}" ${keycloak_url}/admin/realms)
    if [[ $http_code -eq 401 ]]; then
        return 0
    else
        return 1
    fi
}

# wait for keycloak to initialize before configuring
until is_keycloak_running; do
        echo Keycloak still not running, waiting 5 seconds
        sleep 5
    done

echo Keycloak is running, proceeding with configuration

kcadm_config_connection="--no-config --server ${keycloak_url} --user $KEYCLOAK_ADMIN --realm master --password $KEYCLOAK_ADMIN_PASSWORD"
kcadm_config="--target-realm $REALM_NAME ${kcadm_config_connection}"

kcadm_cmd=/opt/keycloak/bin/kcadm.sh

_existing_realm=$($kcadm_cmd get realms $kcadm_config | jq -r ".[] | select(.realm == \"${REALM_NAME}\").id")

if [ -z "${_existing_realm}" ]; then
    kcadm_realm_cmd="create realms"
else
    kcadm_realm_cmd="update realms/${REALM_NAME}"
fi

$kcadm_cmd ${kcadm_realm_cmd} ${kcadm_config_connection} -f - << EOF
{
   "id": "${REALM_NAME}",
   "realm": "${REALM_NAME}",
   "enabled": true,
   "resetPasswordAllowed": true,
   "smtpServer": {
     "host": "${SMTP_HOST}",
     "port": "${SMTP_PORT}",
     "from": "${SMTP_FROM}"
   },
   "roles": {
      "realm": [
         { "name": "$AUTH_USER_ROLE" },
         { "name": "$USER_MANAGE_ROLE" }
      ]
   }
} 
EOF

# 1. Create keycloak group

_existing_group=$($kcadm_cmd get groups $kcadm_config | jq -r '.[] | select(.path == "/DICOM").id')

if [ -z "${_existing_group}" ]; then
  $kcadm_cmd create groups ${kcadm_config} -f - << EOF
{
  "name": "DICOM",
  "path": "/DICOM"
}
EOF
fi

_existing_group=$($kcadm_cmd get groups $kcadm_config | jq -r '.[] | select(.path == "/DICOM").id')

role_id=$($kcadm_cmd get roles/${AUTH_USER_ROLE} ${kcadm_config} | jq -r '.id')

$kcadm_cmd create groups/${_existing_group}/role-mappings/realm ${kcadm_config} -f - << EOF
[{
    "id":"${role_id}",
    "name":"${AUTH_USER_ROLE}",
    "composite":false,
    "clientRole":false,
    "containerId":"${REALM_NAME}"
}]
EOF

#  Turn USER_MANAGE_ROLE into a composite role which allows managing realm users
#  (Assign view-users, query-users, manage-users client roles from realm-management client)

user_manage_role_id=$($kcadm_cmd get roles/${USER_MANAGE_ROLE} ${kcadm_config} | jq -r '.id')
realm_management_client_id=$($kcadm_cmd get clients $kcadm_config | jq -r ".[] | select(.clientId == \"realm-management\").id")

$kcadm_cmd update roles-by-id/${user_manage_role_id} ${kcadm_config} -f - << EOF
{
    "name": "${USER_MANAGE_ROLE}",
    "composite": true,
    "clientRole": false,
    "containerId": "${REALM_NAME}"
}
EOF

declare -a realm_management_roles=("view-users" "query-users" "manage-users")
for name in "${realm_management_roles[@]}"; do
realm_management_role_id=$($kcadm_cmd get clients/${realm_management_client_id}/roles ${kcadm_config} | jq -r ".[] | select(.name == \"${name}\").id")

$kcadm_cmd create roles-by-id/${user_manage_role_id}/composites ${kcadm_config} -f - << EOF
[{
    "clientRole": true,
    "composite": true,
    "containerId": "${realm_management_client_id}",
    "id": "${realm_management_role_id}",
    "name": "${name}"
}]
EOF

done

# 1. Create UI and RS clients

_existing_id=$($kcadm_cmd get clients $kcadm_config | jq -r ".[] | select(.clientId == \"${UI_CLIENT_ID}\").id")

[ -n "$_existing_id" ] && client_command="update clients/$_existing_id" || client_command="create clients"

$kcadm_cmd ${client_command} ${kcadm_config} -f - << EOF
{
    "clientId": "$UI_CLIENT_ID",
    "rootUrl": "${ARC_URL}/dcm4chee-arc/ui2",
    "baseUrl": "${ARC_URL}/dcm4chee-arc/ui2",
    "redirectUris": [
        "${ARC_URL}/dcm4chee-arc/ui2/*"
    ],
    "webOrigins": [
        "${ARC_URL}"
    ],
    "publicClient": true,
    "directAccessGrantsEnabled": true,
    "protocolMappers" : [ {
    "name" : "access_control",
    "protocol" : "openid-connect",
    "protocolMapper" : "oidc-usermodel-attribute-mapper",
    "consentRequired" : false,
    "config" : {
      "aggregate.attrs" : "true",
      "multivalued" : "true",
      "userinfo.token.claim" : "true",
      "user.attribute" : "sAMAccountName",
      "id.token.claim" : "false",
      "access.token.claim" : "false",
      "claim.name" : "access_control"
    }
  } ]
}
EOF

_existing_id=$($kcadm_cmd get clients $kcadm_config | jq -r ".[] | select(.clientId == \"${RS_CLIENT_ID}\").id")

[ -n "$_existing_id" ] && client_command="update clients/$_existing_id" || client_command="create clients"

$kcadm_cmd ${client_command} ${kcadm_config} -f - << EOF
{
  "clientId": "$RS_CLIENT_ID",
  "rootUrl": "${ARC_URL}/dcm4chee-arc",
  "baseUrl": "${ARC_URL}/dcm4chee-arc",
  "redirectUris": [
      "${ARC_URL}/dcm4chee-arc/*"
  ],
  "webOrigins": [
      "${ARC_URL}"
  ],
  "publicClient": true,
  "directAccessGrantsEnabled": true,
  "protocolMappers" : [ {
    "name" : "access_control",
    "protocol" : "openid-connect",
    "protocolMapper" : "oidc-usermodel-attribute-mapper",
    "consentRequired" : false,
    "config" : {
      "aggregate.attrs" : "true",
      "multivalued" : "true",
      "userinfo.token.claim" : "true",
      "user.attribute" : "sAMAccountName",
      "id.token.claim" : "false",
      "access.token.claim" : "false",
      "claim.name" : "access_control"
    }
  } ]
}
EOF


_existing_id=$($kcadm_cmd get clients $kcadm_config | jq -r ".[] | select(.clientId == \"${MGMT_ID}\").id")

[ -n "$_existing_id" ] && client_command="update clients/$_existing_id" || client_command="create clients"

$kcadm_cmd ${client_command} ${kcadm_config} -f - << EOF
{
  "clientId": "$MGMT_ID",
  "rootUrl": "${MGMT_URL}",
  "baseUrl": "${MGMT_URL}",
  "redirectUris": [
      "${MGMT_URL}/*",
      "http://127.0.0.1:5000/*",
      "http://127.0.0.1:5555/*"
  ],
  "webOrigins": [
      "${MGMT_URL}",
      "http://127.0.0.1:5000",
      "http://127.0.0.1:5555"
  ],
  "secret": "11111111-2222-3333-4444-555555555555",
  "clientAuthenticatorType": "client-secret",
  "enabled": true,
  "publicClient": false,
  "directAccessGrantsEnabled": false,
  "protocol": "openid-connect",
  "attributes": {
      "post.logout.redirect.uris" : "${MGMT_URL}/deauthorize##http://127.0.0.1:5000/deauthorize##http://127.0.0.1:5555/deauthorize"
  }
}
EOF

# 2. Create Wildfly console client

_existing_id=$($kcadm_cmd get clients $kcadm_config | jq -r ".[] | select(.clientId == \"${WILDFLY_CONSOLE_ID}\").id")


[ -n "$_existing_id" ] && client_command="update clients/$_existing_id" || client_command="create clients"

$kcadm_cmd ${client_command} ${kcadm_config} -f - << EOF
{
  "clientId": "$WILDFLY_CONSOLE_ID",
  "rootUrl": "https://${KC_HOSTNAME}:9993/console",
  "baseUrl": "https://${KC_HOSTNAME}:9993/console",
  "redirectUris": [
      "https://${KC_HOSTNAME}:9993/console/*",
      "https://localhost:9993/console/*"
  ],
  "webOrigins": [
      "https://${KC_HOSTNAME}:9993",
      "https://localhost:9993"
  ],
  "publicClient": true,
  "directAccessGrantsEnabled": true
}
EOF

# 3. Replace existing ldap provider ID with custom one

existing_ldap_provider_id=$($kcadm_cmd get components $kcadm_config |jq -r \
 '.[] | select((.name == "ldap") and .providerType == "org.keycloak.storage.UserStorageProvider").id')


declare -a providers=("keycloak-ldap" "keycloak-ldap-local")
for name in "${providers[@]}"
do

if [[ "${name}" == "keycloak-ldap" ]]; then
   BASE=""
   EDIT_MODE="READ_ONLY"
else
   BASE="ou=dynamic,"
   EDIT_MODE="WRITABLE"
fi

_existing_id=$($kcadm_cmd get components $kcadm_config |jq -r \
 ".[] | select((.name == \"${name}\") and .providerType == \"org.keycloak.storage.UserStorageProvider\").id")

[ -n "$_existing_id" ] && client_command="update components/$_existing_id" || client_command="create components"

$kcadm_cmd ${client_command} ${kcadm_config} -f - << EOF
{
  "providerId": "ldap",
  "providerType": "org.keycloak.storage.UserStorageProvider",
  "parentId": "$REALM_NAME",
  "name": "${name}",
  "config": {
    "enabled": [
      "true"
    ],
    "usersDn": [
      "ou=people,${BASE}${LDAP_BASE_DN}"
    ],
    "bindDn": [
      "cn=admin,${LDAP_BASE_DN}"
    ],
    "bindCredential": [
      "secret"
    ],
    "usernameLDAPAttribute": [
      "cn"
    ],
    "vendor": [
      "other"
    ],
    "uuidLDAPAttribute": [
      "entryUUID"
    ],
    "connectionUrl": [
      "ldap://ldap:389"
    ],
    "authType": [
      "simple"
    ],
    "searchScope": [
      "1"
    ],
    "userObjectClasses": [
      "user, organizationalPerson"
    ],
    "rdnLDAPAttribute": [
      "cn"
    ],
    "editMode": [
      "${EDIT_MODE}"
    ]
  }
}
EOF

[ -n "$_existing_id" ] && client_command="update components/$_existing_id" || client_command="create components"
new_ldap_provider_id=$($kcadm_cmd get components $kcadm_config |jq -r \
 ".[] | select((.name == \"${name}\") and .providerType == \"org.keycloak.storage.UserStorageProvider\").id")

$kcadm_cmd create components ${kcadm_config} -f - << EOF
{
  "name": "services",
  "providerId": "role-ldap-mapper",
  "providerType": "org.keycloak.storage.ldap.mappers.LDAPStorageMapper",
  "parentId": "${new_ldap_provider_id}",
  "config": {
    "membership.attribute.type": [
      "DN"
    ],
    "roles.dn": [
      "ou=Managed,ou=groups,ou=dynamic,${LDAP_BASE_DN}"
    ],
    "user.roles.retrieve.strategy": [
      "LOAD_ROLES_BY_MEMBER_ATTRIBUTE"
    ],
    "mode": [
      "READ_ONLY"
    ],
    "membership.ldap.attribute": [
      "member"
    ],
    "membership.user.ldap.attribute": [
      "cn"
    ],
    "memberof.ldap.attribute": [
      "memberOf"
    ],
    "role.name.ldap.attribute": [
      "cn"
    ],
    "use.realm.roles.mapping": [
      true
    ],
    "role.object.classes": [
      "Group"
    ]
  }
}
EOF

$kcadm_cmd create components ${kcadm_config} -f - << EOF
{
  "name": "arc-access",
  "providerId": "group-ldap-mapper",
  "providerType": "org.keycloak.storage.ldap.mappers.LDAPStorageMapper",
  "parentId": "${new_ldap_provider_id}",
  "config": {
    "membership.user.ldap.attribute": [
      "cn"
    ],
    "user.roles.retrieve.strategy": [
      "LOAD_GROUPS_BY_MEMBER_ATTRIBUTE"
    ],
    "membership.ldap.attribute": [
      "member"
    ],
    "memberof.ldap.attribute": [
      "memberOf"
    ],
    "membership.attribute.type": [
      "DN"
    ],
    "preserve.group.inheritance": [
      false
    ],
    "groups.dn": [
      "ou=DICOM,ou=groups,ou=dynamic,${LDAP_BASE_DN}"
    ],
    "group.name.ldap.attribute": [
      "sAMAccountName"
    ],
    "mapped.group.attributes": [
      "sAMAccountName"
    ],
    "mode": [
      "READ_ONLY"
    ],
    "ignore.missing.groups": [
      true
    ],
    "group.object.classes": [
      "group"
    ],
    "drop.non.existing.groups.during.sync": [
      true
    ],
    "groups.path": [
      "/DICOM"
    ]
  }
}
EOF

done

[ -n "$existing_ldap_provider_id" ] && $kcadm_cmd delete components/${existing_ldap_provider_id} ${kcadm_config}

# 5. In order for ui2 to work, token received by user needs permission to view account profile. Create a client role.

account_client_id=$($kcadm_cmd get clients ${kcadm_config} | jq -r '.[] | select(.clientId == "account").id')

view_profile_role_id=$($kcadm_cmd get clients/${account_client_id}/roles ${kcadm_config} | jq -r '.[] | select(.name == "view-profile").id')

if [[ -z "$view_profile_role_id" ]]; then

$kcadm_cmd create clients/${account_client_id}/roles ${kcadm_config} -f - << EOF
{"name":"view-profile"}
EOF
view_profile_role_id=$($kcadm_cmd get clients/${account_client_id}/roles ${kcadm_config} | jq -r '.[] | select(.name == "view-profile").id')

fi

default_roles_dcm4che_id=$($kcadm_cmd get roles/default-roles-${REALM_NAME} ${kcadm_config} | jq -r '.id')

_existing_id=$($kcadm_cmd get roles-by-id/${default_roles_dcm4che_id}/composites ${kcadm_config} | jq -r '.[] | select(.name == "view-profile").id')

if [[ -z "${_existing_id}" ]]; then

$kcadm_cmd create roles-by-id/${default_roles_dcm4che_id}/composites ${kcadm_config} -f - << EOF
[{
    "clientRole": true,
    "composite": false,
    "containerId": "${account_client_id}",
    "id": "${view_profile_role_id}",
    "name": "view-profile"
  }]
EOF

fi

echo "==== Finished setting up keycloak Docker instance ===="
