#!/bin/bash
set -e

if [ ! -f ~/setup.done ]; then
  openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -out /opt/keycloak/conf/selfsigned.cert.pem -keyout /opt/keycloak/conf/selfsigned.key.pem -subj "/CN=${KC_HOSTNAME}/O=Docker/C=CA"
  /setup.sh &
  touch ~/setup.done
fi

exec /opt/keycloak/bin/kc.sh start-dev
